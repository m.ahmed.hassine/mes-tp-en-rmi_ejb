package fr.afcepf.ai107.rmi.entity;

import java.io.Serializable;

public class Toto implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String surname;
	
	public Toto(Integer id, String name, String surname) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
	}
	public Toto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	
	
	
	
}
