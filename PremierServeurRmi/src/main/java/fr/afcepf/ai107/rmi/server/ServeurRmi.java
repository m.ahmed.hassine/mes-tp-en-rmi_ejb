package fr.afcepf.ai107.rmi.server;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import fr.afcepf.ai107.rmi.api.ICalcul;
import fr.afcepf.ai107.rmi.impl.Calcul;

public class ServeurRmi {

	public static void main(String[] args) {
		try {
			LocateRegistry.createRegistry(1234);
			ICalcul objetPublie = new Calcul();
			Naming.bind("rmi://127.0.0.1:1234/objCalcul", objetPublie);
			System.out.println("Objet distant disponible à cette adresse: rmi://127.0.0.1:1234/objCalcul");
			
		} catch (RemoteException | MalformedURLException | AlreadyBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
