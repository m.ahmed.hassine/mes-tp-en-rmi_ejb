package fr.afcepf.ai107.rmi.impl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import fr.afcepf.ai107.rmi.api.ICalcul;
import fr.afcepf.ai107.rmi.entity.Toto;

public class Calcul extends UnicastRemoteObject implements ICalcul {

	private static final long serialVersionUID = 1L;

	public Calcul() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int add(int i, int j) throws RemoteException {
		System.out.println("Coucou exécution du traitement");
		return i + j;
	}

	@Override
	public double pow(double a, double b) throws RemoteException {
		// TODO Auto-generated method stub
		return Math.pow(a, b);
	}

	@Override
	public Toto getToto() throws RemoteException {
		Toto toto = new Toto(1, "Toto", "de Toto");
		return toto;
	}



}
