package fr.afcepf.ai107.appcat.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "toy")
public class Toy implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    private String libelle;
    
    private String couleur;
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Cat cat;

    public Toy(Integer id, String libelle, String couleur, Cat cat) {
        super();
        this.id = id;
        this.libelle = libelle;
        this.couleur = couleur;
        this.cat = cat;
    }

    public Toy() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
    
    
}
