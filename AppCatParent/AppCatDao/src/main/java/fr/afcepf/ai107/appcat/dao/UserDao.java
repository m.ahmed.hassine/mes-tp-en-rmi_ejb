package fr.afcepf.ai107.appcat.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appcat.entity.User;
import fr.afcepf.ai107.appcat.idao.UserIDao;

@Remote (UserIDao.class)
@Stateless
public class UserDao extends GenericDao<User> implements UserIDao{
	
	@PersistenceContext (unitName = "AppCatPU")
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public Boolean exist(User user) {
		Query query = em.createQuery("SELECT u FROM User u WHERE u.login = :paramLogin");
		query.setParameter("paramLogin", user.getLogin());
		List<User> users = query.getResultList();
		return users.size() > 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User authenticate(String login, String password) {
		User user = null;
		List<User> users = null;
		Query query = em.createQuery("SELECT u FROM User u WHERE u.login = :paramLogin AND u.password = :paramPassword");
		query.setParameter("paramLogin", login);
		query.setParameter("paramPassword", password);
		users = query.getResultList();
		if(users.size() > 0) {
			user = users.get(0);
		}
		return user;
	}


}