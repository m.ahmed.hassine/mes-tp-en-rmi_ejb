package fr.afcepf.ai107.appcat.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appcat.entity.Cat;
import fr.afcepf.ai107.appcat.entity.User;
import fr.afcepf.ai107.appcat.idao.CatIDao;

@Remote(CatIDao.class)
@Stateless
public class CatDao extends GenericDao<Cat> implements CatIDao {
	
	@PersistenceContext (unitName = "AppCatPU")
	private EntityManager em;

	@Override
	public List<Cat> getCatsByUser(User user) {
		List<Cat> cats = null;
		Query query = em.createQuery("SELECT c FROM Cat c WHERE c.user.id = :paramIdUser"); 
		query.setParameter("paramIdUser", user.getId());
		cats = query.getResultList();
		return cats;
	}



}
