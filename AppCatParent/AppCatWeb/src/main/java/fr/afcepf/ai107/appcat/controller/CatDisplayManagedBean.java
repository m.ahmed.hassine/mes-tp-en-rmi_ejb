package fr.afcepf.ai107.appcat.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appcat.entity.Cat;
import fr.afcepf.ai107.appcat.entity.User;
import fr.afcepf.ai107.appcat.ibusiness.CatManagementIBusiness;

@ManagedBean (name = "mbCat")
@RequestScoped
public class CatDisplayManagedBean {
	
	private List<Cat> cats;
	
	@ManagedProperty (value = "#{mbAccount.user}" )
	private User user;
	
	@EJB
	private CatManagementIBusiness proxyCatBu;
	
	@PostConstruct
	public void init() {
		cats = proxyCatBu.findCatByUser(user);
	}

	public List<Cat> getCats() {
		return cats;
	}

	public void setCats(List<Cat> cats) {
		this.cats = cats;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	

}
