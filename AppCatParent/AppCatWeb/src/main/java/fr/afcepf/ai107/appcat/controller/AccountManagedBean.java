package fr.afcepf.ai107.appcat.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.afcepf.ai107.appcat.entity.User;
import fr.afcepf.ai107.appcat.ibusiness.AccountIBusiness;

@ManagedBean (name = "mbAccount")
@SessionScoped
public class AccountManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private User user = new User();
	private String message;
	private List<User> users;
	private String login;
	private String password;
	
	@EJB
	private AccountIBusiness proxyAccountBusiness;
	
	public String createAccount() {
		user = proxyAccountBusiness.createAccount(user);
		if(user == null) {
			message = "Ce login existe déjà; choisissez-en un autre";
			user = new User();
		}else {
			message = "Merci " + user.getLogin() + "! Votre compte a bien été créé";
		}
		
		return "/accountCreation.xhtml?faces-redirect=true";
	}
	
	public String connection() {
		user = proxyAccountBusiness.connection(login, password);
		String retour = null;
		if(user != null) {
			retour = "/connectedPage.xhtml?faces-redirect=true";
		}else {
			retour = "/connectionPage.xhtml?faces-redirect=true";
		}		
		return retour;
	}
	
	@PostConstruct
	public void init() {
		users = proxyAccountBusiness.findAll();
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
