package fr.afcepf.ai107.appcat.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appcat.entity.Cat;
import fr.afcepf.ai107.appcat.entity.User;
import fr.afcepf.ai107.appcat.ibusiness.CatManagementIBusiness;
import fr.afcepf.ai107.appcat.idao.CatIDao;

@Remote (CatManagementIBusiness.class)
@Stateless
public class CatManagementBusiness implements CatManagementIBusiness {
	
	@EJB
	private CatIDao proxyCatDao;

	@Override
	public List<Cat> findCatByUser(User user) {
		// TODO Auto-generated method stub
		return proxyCatDao.getCatsByUser(user);
	}

}
