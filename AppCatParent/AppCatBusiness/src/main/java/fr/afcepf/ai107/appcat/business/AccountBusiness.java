package fr.afcepf.ai107.appcat.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appcat.entity.User;
import fr.afcepf.ai107.appcat.ibusiness.AccountIBusiness;
import fr.afcepf.ai107.appcat.idao.UserIDao;

@Remote(AccountIBusiness.class)
@Stateless
public class AccountBusiness implements AccountIBusiness {

	@EJB
	private UserIDao proxyUserDao;
	
	
	@Override
	public User createAccount(User user) {
		User userReturned = null;
		if(!proxyUserDao.exist(user)) {
			userReturned = proxyUserDao.add(user);
		}
		return userReturned;
	}


	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return proxyUserDao.findAll();
	}


	@Override
	public User connection(String login, String password) {
		// TODO Auto-generated method stub
		return proxyUserDao.authenticate(login, password);
	}

}
