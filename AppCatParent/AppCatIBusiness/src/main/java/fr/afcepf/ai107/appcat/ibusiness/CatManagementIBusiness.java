package fr.afcepf.ai107.appcat.ibusiness;

import java.util.List;

import fr.afcepf.ai107.appcat.entity.Cat;
import fr.afcepf.ai107.appcat.entity.User;

public interface CatManagementIBusiness {
	List<Cat> findCatByUser (User user);
}
