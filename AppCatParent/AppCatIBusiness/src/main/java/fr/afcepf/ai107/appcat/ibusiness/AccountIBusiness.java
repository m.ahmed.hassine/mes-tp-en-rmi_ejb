package fr.afcepf.ai107.appcat.ibusiness;

import java.util.List;

import fr.afcepf.ai107.appcat.entity.User;

public interface AccountIBusiness {
	User createAccount (User user);
	List<User> findAll();
	User connection(String login, String password);
}
