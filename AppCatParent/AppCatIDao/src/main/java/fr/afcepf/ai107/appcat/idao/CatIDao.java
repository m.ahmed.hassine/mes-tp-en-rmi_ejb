package fr.afcepf.ai107.appcat.idao;

import java.util.List;

import fr.afcepf.ai107.appcat.entity.Cat;
import fr.afcepf.ai107.appcat.entity.User;

public interface CatIDao extends GenericIDao<Cat> {
	List<Cat> getCatsByUser(User user);

}
