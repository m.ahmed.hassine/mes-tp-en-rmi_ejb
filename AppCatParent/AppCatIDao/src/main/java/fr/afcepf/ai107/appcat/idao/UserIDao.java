package fr.afcepf.ai107.appcat.idao;

import fr.afcepf.ai107.appcat.entity.User;

public interface UserIDao extends GenericIDao<User>{
	Boolean exist(User user);
	User authenticate(String login, String password);
}
