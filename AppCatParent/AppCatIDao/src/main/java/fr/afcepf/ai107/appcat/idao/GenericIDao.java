package fr.afcepf.ai107.appcat.idao;

import java.util.List;

public interface GenericIDao<T> {
	T add(T t);
	boolean delete(T t);
	T update(T t);
	T getById(int i);
	List<T> findAll();

}
