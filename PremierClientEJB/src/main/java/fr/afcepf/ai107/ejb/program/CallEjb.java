package fr.afcepf.ai107.ejb.program;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.afcepf.ai107.ejb.api.ICalcul;

public class CallEjb {

	public static void main(String[] args) {
		//Dans un context inter au serveur, tout ça sera remplacé par @EJB
		Properties props = new Properties();
		props.put(Context.PROVIDER_URL, "http-remoting://127.0.0.1:8082");
		props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		props.put("jboss.naming.client.ejb.context", true);
		
		try {
			Context ctx = new InitialContext(props);
			ICalcul proxy = (ICalcul) ctx.lookup("PremierEJB/Calcul!fr.afcepf.ai107.ejb.api.ICalcul");
			System.out.println(proxy.add(40, 2));
			System.out.println(proxy.pow(10, 2));
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
