package fr.afcepf.ai107.rmi.program;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import fr.afcepf.ai107.rmi.api.ICalcul;
import fr.afcepf.ai107.rmi.entity.Toto;

public class CallRmi {

	public static void main(String[] args) {
		String JNDI = "rmi://127.0.0.1:1234/objCalcul";
		try {
			ICalcul proxy = (ICalcul) Naming.lookup(JNDI);
			System.out.println(proxy.add(40, 2));
			Toto leToto = proxy.getToto();
			System.out.println("le Toto : " + leToto.getName() + " " + leToto.getSurname());
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
