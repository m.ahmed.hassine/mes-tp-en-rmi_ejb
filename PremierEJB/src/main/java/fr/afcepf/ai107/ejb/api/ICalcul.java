package fr.afcepf.ai107.ejb.api;

public interface ICalcul {
	int add(int i, int j);
	double pow(double a, double b);
}
