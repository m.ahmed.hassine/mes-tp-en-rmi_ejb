package fr.afcepf.ai107.ejb.impl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.ejb.api.ICalcul;

@Remote (ICalcul.class)
@Stateless
public class Calcul implements ICalcul {

	@Override
	public int add(int i, int j) {
		// TODO Auto-generated method stub
		return i +j ;
	}

	@Override
	public double pow(double a, double b) {
		// TODO Auto-generated method stub
		return Math.pow(a,b);
	}

}
